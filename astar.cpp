#include<iostream>
#include<algorithm>
#include<vector>
#include<set>
#include<cstdlib>
int sample[5][5] = {{1, 0, 1, 1, 1},
					{1, 1, 1, 0, 1},
					{1, 0, 0, 0, 1},
					{1, 0, 0, 0, 1},
					{1, 1, 1, 1, 1}};


enum E_NODE_DIRECTION {
	N = 1,
	S,
	E,
	W,
	NE,
	NW,
	SE,
	SW
};

struct NodeProxy {
	int x;
	int y;
};

class FringeNode {
	public:
		NodeProxy * node;
		std::vector<E_NODE_DIRECTION> path;
		NodeProxy *destination;
		
		FringeNode(NodeProxy *node, std::vector<E_NODE_DIRECTION> path, NodeProxy* destination) {
			this->node = node;
			this->path = path;
			this->destination = destination;
		};
		void PrintCoordinates (NodeProxy *start) {
			int x=start->x;
			int y=start->y;
			using namespace std;
			for (std::vector<E_NODE_DIRECTION>::iterator it=path.begin();it!=path.end();++it) {
				switch(*it) {
					case N:
				 		y -= 1;
				 		break;
		 			case S:
		 				y+= 1;
		 				break;
	 				case E:
	 					x += 1;
	 					break;
 					case W:
 						x-=1;
 						break;
					default:
						cout<<"wtf"<<endl;
				}
				cout<<x<<":"<<y<<endl;
			}
		}
		int manhattanDistance () {
			return abs(node->x - destination->x) + abs(node->y-destination->y);
		}	
		
		int GetCostOfPath () {
			int cost = manhattanDistance()*10;
			for (std::vector<E_NODE_DIRECTION>::iterator it = path.begin(); it!=path.end(); ++it) {
				switch (*it) {
					case N:
					case S:
					case E:
					case W:
						cost += 10;
						break;
					case NE:
					case NW:
					case SE:
					case SW:
						cost += 14;
						break;
				}
			}
			return cost;
		}
		
		NodeProxy * GetNode() {
			return this->node;
		}
		
		void printMe() {
			std::cout<<node->x<<","<<node->y<<" ->";
			for (std::vector<E_NODE_DIRECTION>::iterator it = path.begin(); it!=path.end(); ++it) {
				switch(*it) {
					case N:
						std::cout<<"N";
						break;
					case S:
						std::cout<<"S";
						break;
					case E:
						std::cout<<"E";
						break;
					case W:
						std::cout<<"W";
						break;
					default: 
						std::cout<<*it;
						break;
				}
			}
			std::cout<<" cost: "<<GetCostOfPath();
			std::cout<<std::endl;
		}

};

class CollisionGraph {
	public:
		NodeProxy** nodes;
		int **AdjacencyMatrix;
		int XMax, YMax;
		CollisionGraph (int *m[], int XMax, int YMax) {
			AdjacencyMatrix = m;
			this->XMax = XMax;
			this->YMax = YMax;
			nodes = new NodeProxy*[XMax];
			for (int i=0; i<XMax; i++) {
				nodes[i] = new NodeProxy[YMax];
				for (int j=0; j<YMax; j++) {
					nodes[i][j].x = i;
					nodes[i][j].y = j;
					std::cout<<m[i][j]<<" ";
				}
				std::cout<<std::endl;
			}
		};
		NodeProxy * GetNode (int x, int y) {
			return &nodes[x][y];
		}
		std::vector<FringeNode> GetFringeNeighbours(NodeProxy * node, std::vector<E_NODE_DIRECTION> path, NodeProxy * destination) {
			int x = node->x;
			int y = node->y;
			NodeProxy * temp;
			
			std::vector<FringeNode> neighbours;
			if (x+1 < XMax && AdjacencyMatrix[x+1][y] == 1) {
				//East
				temp = GetNode(x+1, y);
				std::vector<E_NODE_DIRECTION> eastPath(path);
				eastPath.push_back(E);
				neighbours.push_back(FringeNode(temp, eastPath, destination));
			}
			if (x-1 >= 0 && AdjacencyMatrix[x-1][y] ==1) {
				//West
				temp = GetNode(x-1, y);
				std::vector<E_NODE_DIRECTION> westPath(path);
				westPath.push_back(W);
				neighbours.push_back(FringeNode(temp, westPath, destination));
			}
			if (y-1 >= 0 && AdjacencyMatrix[x][y-1] == 1) {
				//North
				temp = GetNode(x, y-1);
				std::vector<E_NODE_DIRECTION> northPath(path);
				northPath.push_back(N);
				neighbours.push_back(FringeNode(temp, northPath, destination));
			}
			if (y+1 < YMax && AdjacencyMatrix[x][y+1] == 1) {
				//South
				temp = GetNode(x, y+1);
				std::vector<E_NODE_DIRECTION> southPath(path);
				southPath.push_back(S);
				neighbours.push_back(FringeNode(temp, southPath, destination));
			}
			return neighbours;
		}
};
class ComparatorFunction {
	public:
	bool operator () (FringeNode &n1, FringeNode& n2) {
		return n1.GetCostOfPath() > n2.GetCostOfPath();
	}
};
class AStar {
	private:
		CollisionGraph *cg;
		int ** explored;
		std::vector<FringeNode> fringe;
		int X_MAX, Y_MAX;
		
	public: 
		AStar (CollisionGraph *c, int x, int y) {
			cg = c;
			X_MAX = x;
			Y_MAX = y;
			explored = new int*[y];
			for (int i=0; i<y; i++) {
				explored[i] = new int[x];
				for (int j=0; j<x; j++) {
					explored[i][j]=0;
				}
			}
		}
		
		NodeProxy * NewNode (int x, int y) {
			NodeProxy * temp = new NodeProxy();
			temp->x = x;
			temp->y = y;
      return temp;
		}
		
		void AddNodeToFringe (NodeProxy *node, std::vector<E_NODE_DIRECTION> path, NodeProxy *destination) {
			FringeNode temp(node, path, destination);
			fringe.push_back(temp);
			std::push_heap(fringe.begin(), fringe.end(), ComparatorFunction());
		}
		void AddNodeToFringe (FringeNode fnode) {
			fringe.push_back(fnode);
			std::push_heap(fringe.begin(), fringe.end(), ComparatorFunction());
		}
		
		void PopNodeFromFringe () {
			std::pop_heap(fringe.begin(), fringe.end(), ComparatorFunction());
			fringe.pop_back();
		}
		
		FringeNode GetMinFromFringe () {
			return fringe.front();
		}
		
		FringeNode GetPath (NodeProxy * source, NodeProxy * destination) {
			std::vector<E_NODE_DIRECTION> emptyPath;
			AddNodeToFringe (source, emptyPath, destination);
			
			while (fringe.size() >0) {
				FringeNode current = GetMinFromFringe();
				PopNodeFromFringe();
				current.printMe();
				if (current.GetNode() == destination){//current.GetNode()->x == destination->x && current.GetNode()->y == destination->y) {
					return current;
				}
				else {
					explored[current.GetNode()->x][current.GetNode()->y] =1;
					std::vector<FringeNode> neighbours = cg->GetFringeNeighbours(current.GetNode(), current.path, destination);
					for (std::vector<FringeNode>::iterator neighbour = neighbours.begin(); neighbour!=neighbours.end(); neighbour++) {
						FringeNode node = *neighbour;
						if (explored[node.GetNode()->x][node.GetNode()->y] == 0) {
							AddNodeToFringe(node);
						}
					}
				}
			}
      return FringeNode(source, emptyPath, destination);
		}
			
		
};			 
int main () {
	int **test = new int*[5];
	for (int i=0; i<5;i++) {
		test[i] = new int[5];
		for (int j=0; j<5; j++) {
			test[i][j] = sample[i][j];
		}
	};
	CollisionGraph cg(test, 5, 5);
	NodeProxy *start = cg.GetNode(0, 0);
	NodeProxy *end = cg.GetNode(4, 4);
	AStar as(&cg, 5, 5);
	FringeNode solution = as.GetPath(start, end);
	solution.printMe();
	solution.PrintCoordinates(start);
	return -1;
}


